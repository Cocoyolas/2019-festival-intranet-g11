<?php
/**
 * Description Page de consultation de la liste des groupes
 * -> affiche un tableau constitué d'une ligne d'entête et d'une ligne par groupe
 * @author prof
 * @version 2019
 */
namespace vue\representation;

use vue\VueGenerique;

class VueListeRepresentations extends VueGenerique {
    
    /** @var array iliste des établissements à afficher avec leur nombre d'atttributions */
   private $lesRepresentationsAvecNbAttributions;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille" >

            
            <?php
            // Pour chaque groupe lu dans la base de données
            foreach ($this->lesRepresentationsAvecNbAttributions as $uneRepresentationAvecNbAttrib) {
                $uneRepresentation = $uneRepresentationAvecNbAttrib["rep"];
                 
                 
                ?>
            
                
                
                
            
            <table> 
                <strong> <?= $date = $uneRepresentation->getDate(); ?> </strong>
                <tr class="enTeteTabNonQuad" >
                    <td width="17%">ID</td>

                    <td width="17%">Lieu</td>

                    <td width="17%">Groupe</td>
                    
                    <td width="17%">Heure de début</td>
                    
                    <td width="17%">Heure de fin</td>
                    
                    
                </tr>
            <?php
                $id = $uneRepresentation->getId();
                $idLieu = $uneRepresentation->getIdLieu();
                $idGroupe = $uneRepresentation->getIdGroupe();
                
                $heuredebut = $uneRepresentation->getHeureDebut();
                $heurefin = $uneRepresentation->getHeureFin();
                ?>
                <tr class="ligneTabNonQuad" >
                    <td width="17%" ><?= $id ?></td>

                    <td width="17%"  > <?= $idLieu ?></td>

                    <td width="17%"  > <?= $idGroupe ?> </td>
                    
                    <td width="17%" ><?= $heuredebut ?></td>
                    
                    <td width="17%" ><?= $heurefin ?></td>
                    
                   
                    
            </tr>
            
            <?php
            }
            ?>
            
        </table>
        <br>
        
        <?php
        include $this->getPied();
    }

    function setLesRepresentationsAvecNbAttributions($lesRepresentationsAvecNbAttributions) {
        $this->lesRepresentationsAvecNbAttributions = $lesRepresentationsAvecNbAttributions;
    }

}
