<?php
namespace modele\dao;

use modele\metier\Representation;
use PDOStatement;
use PDO;


/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author cbaudry
 * @version 2019
 */
class RepresentationDAO {


    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table Representation
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $lieu = $enreg['IDLIEU'];
        $groupe = $enreg['IDGROUPE'];
        $date = $enreg['DATE'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        
        $uneRepresentation = new Representation($id, $lieu, $groupe, $date, $heureDebut, $heureFin);

        return $uneRepresentation;
    }
    
    
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param Representation $objetMetier un lieu
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':idLieu', $objetMetier->getIdLieu());
        $stmt->bindValue(':idGroupe', $objetMetier->getIdGroupe());
        $stmt->bindValue(':date', $objetMetier->getDate());
        $stmt->bindValue(':heureDebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heureFin', $objetMetier->getHeureFin());
    }


    /**
     * Retourne la liste de toutes les representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY DATE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

        /**
     * Liste des objets Attribution concernant une représentation donnée
     * @param string $uneDate : date de la représentation dont on filtre les attributions
     * @return array : tableau d'attributions
     */
    public static function getAllByDate($uneDate) {
        $lesRepresentationsParDate = array();
        $requete = "SELECT * FROM Representation WHERE DATE = date ORDER BY date";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':date', $uneDate);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesRepresentationsParDate[] = self::enregVersMetier($enreg);
            }
        }
        return $lesRepresentationsParDate;
    }
    
       /**
     * Retourne la liste de toutes les representations 
     * @return tableau d'attributions
     */
    public static function getAllDate() {
        $lesDates = array();
        $requete = "SELECT Distinct date FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesDates[] = $enreg['DATE'];
            }
        }
        return $lesDates;
    }
    
    /**
     * Retourne la liste de toutes les représentation triées par lieu
     * @return tableau d'attributions
     */
        public static function getAllbyLieu() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY id_lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Retourne la liste de toutes les représentation triées par groupe
     * @return tableau d'attribution
     */
        public static function getAllbyGroupe() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY id_groupe";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    public static function getRepresentationByDate($date) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE DATE = :DATE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':DATE', $date);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }


    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet representation métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation (id,idlieu,idgroupe,date,heureDebut,heureFin) VALUES (:id, :idLieu, :idGroupe, :date, :heureDebut, :heureFin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    
    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET IDLIEU=:idLieu, IDGROUPE=:idGroupe, DATE=:date, HEUREDEBUT=:heureDebut, HEUREFIN=:heureFin"
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Détruire un enregistrement de la table Representation d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non un lieu ayant déjà le même identifiant dans la BD
     * @param string $id identifiant du lieu à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

    
}