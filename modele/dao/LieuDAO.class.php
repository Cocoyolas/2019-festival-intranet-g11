<?php
namespace modele\dao;

use modele\metier\Lieu;
use PDOStatement;
use PDO;

/**
 * Description of LieuDAO
 * Classe métier :  Lieu
 * @author Clément
 * @version 2017
 */
class LieuDAO {


    /**
     * Instancier un objet de la classe Lieu à partir d'un enregistrement de la table Lieu
     * @param array $enreg
     * @return Lieu
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $nom = $enreg['NOM'];
        $adr = $enreg['ADR'];
        $capacite = $enreg['CAPCITE'];
        $unLieu = new Lieu($id, $nom, $adr,$capacite);

        return $unLieu;
    }
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Lieu
     * @param Lieu $objetMetier un lieu
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Lieu $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':nom', $objetMetier->getNom());
        $stmt->bindValue(':adr', $objetMetier->getAdr());
        $stmt->bindValue(':capacite', $objetMetier->getCapacite());
    }


    /**
     * Retourne la liste de tous les Lieux
     * @return array tableau d'objets de type Lieux
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu ORDER BY NOM";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id
     * @return Lieu le lieu trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }


    /**
     * Retourne la liste des groupes attribués à un établissement donné
     * @param string $idEtab
     * @return array tableau d'éléments de type Groupe
     */
//    public static function getAllByEtablissement($idEtab) {
//        $lesGroupes = array();  // le tableau à retourner
//        $requete = "SELECT * FROM Groupe
//                    WHERE ID IN (
//                    SELECT DISTINCT ID FROM Groupe g
//                            INNER JOIN Attribution a ON a.IDGROUPE = g.ID 
//                            WHERE IDETAB=:id
//                    )";
//        $stmt = Bdd::getPdo()->prepare($requete);
//        $stmt->bindParam(':id', $idEtab);
//        $ok = $stmt->execute();
//        if ($ok) {
//            // Tant qu'il y a des enregistrements dans la table
//            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
//                //ajoute un nouveau groupe au tableau
//                $lesGroupes[] = self::enregVersMetier($enreg);
//            }
//        } 
//        return $lesGroupes;
//    }

    
    /**
     * Retourne la liste des groupes souhaitant un hébergement, ordonnée par id
     * @return array tableau d'éléments de type Groupe
     */
//    public static function getAllToHost() {
//        $lesGroupes = array();
//        $requete = "SELECT * FROM Groupe WHERE HEBERGEMENT='O' ORDER BY ID";
//        $stmt = Bdd::getPdo()->prepare($requete);
//        $ok = $stmt->execute();
//        if ($ok) {
//            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
//                $lesGroupes[] = self::enregVersMetier($enreg);
//            }
//        }
//        return $lesGroupes;
//    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Lieu $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Lieu $objet) {
        $requete = "INSERT INTO Lieu VALUES (:id, :nom, :adr, :capacite)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Lieu $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opération échoue
     */
    public static function update($id, Lieu $objet) {
        $ok = false;
        $requete = "UPDATE Lieu SET NOM=:nom, ADR=:adr, CAPACITE=:capacite
           WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    /**
     * Détruire un enregistrement de la table LIEU d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Lieu WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non un lieu ayant déjà le même identifiant dans la BD
     * @param string $id identifiant du lieu à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Lieu WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
    
}
