<?php
namespace modele\metier;

/**
 * Description of Lieu
 * Lieu où se déroule la représentation
 * @author Clément
 */
class Lieu {
    /**
     * id du lieu ("lxxx")
     * @var string
     */
    private $id;
    
    /**
     * nom du lieu
     * @var string
     */
    private $nom;
    
    /**
     * adresse du lieu
     * @var string
     */
    private $adr;
    
    /**
     * capacité maximale du lieu
     * @var integer
     */
    private $capacite;
 
    function __construct($id, $nom, $adr, $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adr = $adr;
        $this->capacite = $capacite;
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdr() {
        return $this->adr;
    }

    function getCapacite() {
        return $this->capacite;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdr($adr) {
        $this->adr = $adr;
    }

    function setCapacite($capacite) {
        $this->capacite = $capacite;
    }
    
}