<?php
namespace modele\metier;

/**
 * Description d'une représentation
 * 
 * @author Clément
 */
class Representation {
    /** id de la représentation
     * @var integer
     */
    private $id;
    
    /**
     * Lieu de la représentation
     * @var string
     */
    private $lieu;
    
    /**
     * Groupe de la représentation
     * @var string
     */
    private $groupe;
    
    /** Date de la représentation
     * @var string
     */
    private $date;
    
    /** Heure de début de la représentation
     * @var string
     */
    private $heureDebut;
    
    /** Heure de fin de la représentation
     * @var string
     */
    private $heureFin;
    
    function __construct($id, $lieu, $groupe, $date, $heureDebut, $heureFin) {
        $this->id = $id;
        $this->lieu = $lieu;
        $this->groupe = $groupe;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
    }
    
    function getId() {
        return $this->id;
    }

    function getIdLieu() {
        return $this->lieu;
    }

    function getIdGroupe() {
        return $this->groupe;
    }

    function getDate() {
        return $this->date;
    }

    function getHeureDebut() {
        return $this->heureDebut;
    }

    function getHeureFin() {
        return $this->heureFin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLieu($lieu) {
        $this->lieu = $lieu;
    }

    function setGroupe($groupe) {
        $this->groupe = $groupe;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }

}