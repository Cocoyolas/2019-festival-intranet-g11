<?php

/**
 * Contrôleur de gestion des établissements
 * @author prof
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\RepresentationDAO;
use modele\dao\AttributionDAO;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representation\VueListeRepresentations;
use vue\representation\VueSaisieRepresentations;


class CtrlRepresentations extends ControleurGenerique {

    /** controleur= groupes & action= defaut
     * Afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupes & action= liste
     * Afficher la liste des groupes      */
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes avec, pour chacun,
        //  son nombre de personnes actuel : 
        //  on ne peut supprimer un groupe que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesRepresentationsAvecNbAttributions($this->getTabRepresentationsAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }
    
    /** controleur= groupes & action=modifier $ id=identifiant du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idRepresentation = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $laRepresentation = RepresentationDAO::getOneById($idRepresentation);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : "  . " (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= egroupes & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un établissement */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['Lieu'], $_REQUEST['idgroupe'], $_REQUEST['date'], $_REQUEST['heuredebut'], $_REQUEST['heurefin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRepresentation($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'établissement
            RepresentationDAO::update($uneRepresentation->getId(), $uneRepresentation);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la representation : " .  " (" . $uneRepresentation->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representations");
            $this->vue->afficher();
        }
    }

      /** Supprimer un établissement d'après son identifiant */
    public function supprimer() {
        $idRepresentation = $_GET["id"];
        $this->vue = new VueSupprimerRepresenation();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRepresentation));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }

     /* supprimer un groupe dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du represenation à supprimer");
        } else {
            // suppression de l'établissement d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=representations&action=liste");
    }
    
    private function verifierDonneesRepresentation(Representation $uneRepresentation, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRepresentation->getId() == "") || $uneRepresentation->getIdLieu() == "" ||
                $uneRepresentation->getIdGroupe() == "" || $uneRepresentation->getDate() == "" || $uneRepresentation->getHeureDebut() == "" || $uneRepresentation->getHeureFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepresentation->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRepresentation->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRepresentation->getId())) {
                    GestionErreurs::ajouter("La representation " . $uneRepresentation->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'un groupe de même nom n'existe pas déjà (id + nom si création)
        if ($uneRepresentation->getId() != "" && RepresentationDAO::isAnExistingId($creation, $uneRepresentation->getId())) {
            GestionErreurs::ajouter("Le groupe " . $uneRepresentation->getId() . " existe déjà");
        }
        
        
    }

   public function getTabRepresentationsAvecNbAttributions(): Array {
        $lesRepresentationsAvecNbAttrib = Array();
        $lesRepresentations = RepresentationDAO::getAll();
        foreach ($lesRepresentations as $uneRepresentation) {
            /* @var Representation $uneRepresentation */
            $lesRepresentationsAvecNbAttrib[$uneRepresentation->getId()]['rep'] = $uneRepresentation;
            
        }
        return $lesRepresentationsAvecNbAttrib;
    }


}
